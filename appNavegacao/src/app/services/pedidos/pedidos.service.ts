import { Pedido} from './../../models/Pedido';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PedidoService {
  private readonly URL_M = "https://3000-yasmimlinda-2egbapi0810-jtvkbrkg4kg.ws-us78.gitpod.io/"
  private readonly URL_Y = "https://3000-yasmimlinda-2egbapi0810-fh8j3thnmfc.ws-us78.gitpod.io/"
  private readonly URL = this.URL_M

  constructor(
    private http: HttpClient
  ) { }

  cadastrarPedido(pedido: Pedido): Observable<any>
  {
    return this.http.post<any>(`${this.URL}pedido`, pedido)
  }
}
