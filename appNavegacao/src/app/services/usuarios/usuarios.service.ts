import { Usuario } from './../../models/Usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UsuariosService {
  private readonly URL_M = "https://3000-yasmimlinda-2egbapi0810-jtvkbrkg4kg.ws-us78.gitpod.io/"
  private readonly URL_Y = "https://3000-yasmimlinda-2egbapi0810-fh8j3thnmfc.ws-us78.gitpod.io"
  private readonly URL = this.URL_M

  constructor(
    private http: HttpClient
  ) { }

  logar(usuario: Usuario): Observable <any> 
  {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }

  cadastrar(usuario: Usuario): Observable<any>
  {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }
}