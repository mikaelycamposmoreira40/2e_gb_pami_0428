import { Funcionario} from './../../models/Funcionario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class FuncionariosService {

  private readonly URL_M = "https://3000-yasmimlinda-2egbapi0810-jtvkbrkg4kg.ws-us78.gitpod.io/"
  private readonly URL_Y = "https://3000-yasmimlinda-2egbapi0810-fh8j3thnmfc.ws-us78.gitpod.io/"
  private readonly URL = this.URL_Y
  constructor(

    private http: HttpClient

  ) { }
  cadastrarFuncionario(funcionario: Funcionario): Observable<any>
  {
    return this.http.post<any>(`${this.URL}funcionario`, funcionario)
  }
}
