import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditorasService {
  private readonly URL_M = "https://3000-yasmimlinda-2egbapi0810-14rxkgaqud1.ws-us77.gitpod.io/"
  private readonly URL_Y = "https://3000-yasmimlinda-2egbapi0810-14rxkgaqud1.ws-us77.gitpod.io/"
  private readonly URL = this.URL_M

  constructor(
    private http: HttpClient
  ) { }

  salvar(editora): Observable<any> {
    return this.http.post<any>(`${this.URL}editora`, editora)
  }
}