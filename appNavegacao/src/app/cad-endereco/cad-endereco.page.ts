import { Component, OnInit } from '@angular/core';
import {Endereco } from 'src/app/models/Endereco';
import { EnderecoService } from 'src/app/services/enderecos/enderecos.service';

@Component({
  selector: 'app-cad-endereco',
  templateUrl: './cad-endereco.page.html',
  styleUrls: ['./cad-endereco.page.scss']
})
export class CadEnderecoPage implements OnInit {

  
  endereco : Endereco

  constructor(
    private enderecosServices : EnderecoService
  ) { 
      this.endereco = new Endereco();
    }
  
    cadastrar(): void{
      this.enderecosServices.cadastrarEndereco(this.endereco).subscribe({
        next:(dados)=>{
          console.log(dados)
        }
      })
    }

  ngOnInit() {
  }
}