import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'detalhes',
    loadChildren: () => import('./detalhes/detalhes.module').then( m => m.DetalhesPageModule)
  },
  {
    path: 'cad-autor',
    loadChildren: () => import('./cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('./cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  },
  {
    path: 'cad-livro',
    loadChildren: () => import('./cad-livro/cad-livro.module').then( m => m.CadLivroPageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'adm-login',
    loadChildren: () => import('./admin/adm-login/adm-login.module').then( m => m.AdmLoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'cad-funcionarios',
    loadChildren: () => import('./admin/usuarios/cad-funcionarios/cad-funcionarios.module').then( m => m.CadFuncionariosPageModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./admin/usuarios/usuarios.module').then( m => m.UsuariosPageModule)
  },
  {
    path: 'cad-pedidos',
    loadChildren: () => import('./cad-pedidos/cad-pedidos.module').then( m => m.CadPedidosPageModule)
  },
  {
    path: 'cad-endereco',
    loadChildren: () => import('./cad-endereco/cad-endereco.module').then( m => m.CadEnderecoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
