export class ItemPedido{
    private _id_pedido : number;
    private _id_livro : number;
    private _preco_pago: string;
    private _quantidade: string;
  
    constructor(id_pedido: number, id_livro: number, preco_pago: string, quantidade: string){
        this._id_pedido = id_pedido;
        this._id_livro = id_livro;
        this._preco_pago = preco_pago;
        this._quantidade = quantidade;
    }
    
    public set id_pedido(id_pedido : number) {
        this._id_pedido = id_pedido;
    }
    
    public set id_livro(id_livro : number) {
        this._id_livro = id_livro;
    }
    
    public set preco_pago(preco_pago : string) {
        this._preco_pago = preco_pago;
    }
    
    public set quantidade(quantidade : string) {
        this._quantidade = quantidade;
    }
    
    public get id_pedido() : number {
        return this._id_pedido;
    }
    
    public get id_livro() : number {
        return this._id_livro;
    }
    
    public get preco_pago() : string {
        return this._preco_pago;
    }
    
    public get quantidade() : string {
        return this._quantidade;
    }
    
    
    
    

}