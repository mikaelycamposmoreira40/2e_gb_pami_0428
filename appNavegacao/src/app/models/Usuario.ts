import { Pessoa } from './Pessoa'

export class Usuario extends Pessoa {
    username: string
    password: string
    ativo: boolean
    tipo: string

    constructor()
    {
        super()
        this.username = "";
        this.password = "";
        this.ativo = true;
        this.tipo = "";
    }
}