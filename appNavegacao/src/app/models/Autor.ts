export class Autor {

    private  _nome: string;
    private _nacionalidade: string;
    private _sexo: string;
    private _dataNascimento: Date;

    constructor(){
        this._nome = "";
        this._nacionalidade = "";
        this._sexo = "";
        this._dataNascimento = new Date();
    }

}