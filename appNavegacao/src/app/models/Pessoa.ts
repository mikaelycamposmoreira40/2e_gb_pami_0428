export class Pessoa {
    id: number
    nome: string
    telefone: string
    rua: string
    numero: string
    bairro: string
    cep: string
    cidade: string
    uf: string
    tipo: string
    constructor()
    {
        this.id=0
        this.nome=""
        this.telefone=""
        this.rua=""
        this.numero=""
        this.bairro=""
        this.cep=""
        this.cidade=""
        this.uf=""
        this.tipo=""
    }
}