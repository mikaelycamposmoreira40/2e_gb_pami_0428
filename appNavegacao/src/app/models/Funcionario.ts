export class Funcionario {
    data_admissao: Date;
    salario : number;
    cargo : string;

    constructor(){
        this.data_admissao = new Date();
        this.salario = 0;
        this.cargo = "";
    }
}