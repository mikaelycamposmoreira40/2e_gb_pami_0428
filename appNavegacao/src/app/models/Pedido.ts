type PedidoForma_Pagamento = 'À vista' | 'Pix' | 'À vista - Boleto' | 'À vista - Débito' |  '1 x Crédito (5% de desconto)' |  '3 x Crédito (2% de desconto)' |  '12 x Crédito (sem juros)'

type PedidoStatus = 'Aguardando pagamento'| 'Pagamento confirmado'| 'Preparação de envio'| 'Em transporte'| 'Pedido entregue'| 'Devolução'| 'Em análise'| 'Cancelado'
 
export class Pedido{
    private id_cliente: number;
    private data_pedido: Date;
    private previsao_entrega: Date;
    private forma_pagamento: PedidoForma_Pagamento;
    private observacoes: string;
    private status: PedidoStatus

    constructor()
    {
        this.id_cliente = 0
        this.data_pedido = new Date ()
        this.previsao_entrega = new Date ()
        this.forma_pagamento = null
        this.observacoes = ""
        this.status = null
    }
}