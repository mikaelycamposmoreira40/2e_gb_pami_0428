export class Livro {
    private titulo: string;
    private resumo: string;
    private descricao: string;
    private paginas: number;
    private preco: number;
    private edicao: number;
    private idioma: string;
    private ano_lancamento: number;
    private id_autor: number;
    private id_editor: number;

    constructor()
    {
        this.titulo = "";
        this.resumo = "";
        this.descricao = "";
        this.paginas = 0;
        this.preco = 0;
        this.edicao = 0;
        this.idioma = "";
        this.ano_lancamento = 0;
        this.id_autor = 0;
        this.id_editor = 0;
    }
}