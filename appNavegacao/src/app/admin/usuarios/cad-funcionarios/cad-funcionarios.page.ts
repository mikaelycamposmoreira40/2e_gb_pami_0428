import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/Funcionario';
import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';

@Component({
  selector: 'app-cad-funcionarios',
  templateUrl: './cad-funcionarios.page.html',
  styleUrls: ['./cad-funcionarios.page.scss'],
})
export class CadFuncionariosPage implements OnInit {

  funcionario : Funcionario

  constructor(
    private funcionarioServices : FuncionariosService
  ) { 
      this.funcionario = new Funcionario()
    }
  
    cadastrar(): void{
      this.funcionarioServices.cadastrarFuncionario(this.funcionario).subscribe({
        next:(dados)=>{
          console.log(dados)
        }
      })
    }

  ngOnInit() {
  }

}
