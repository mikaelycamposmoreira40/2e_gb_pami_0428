import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdmLoginPageRoutingModule } from './adm-login-routing.module';

import { AdmLoginPage } from './adm-login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdmLoginPageRoutingModule
  ],
  declarations: [AdmLoginPage]
})
export class AdmLoginPageModule {}
