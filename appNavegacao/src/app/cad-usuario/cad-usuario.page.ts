import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/Usuario';
import { UsuariosService } from '../services/usuarios/usuarios.service';


@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.page.html',
  styleUrls: ['./cad-usuario.page.scss'],
})
export class CadUsuarioPage implements OnInit {
  
  usuario: Usuario

  constructor(
    private usuarioServices: UsuariosService
  ) {
    this.usuario = new Usuario();
  }

  ngOnInit() {
    setInterval(() => {
      console.log(this.usuario)
    }, 1500);
  }

  cadastrar(): void {
    this.usuarioServices.cadastrar(this.usuario).subscribe(
      {
        next: (dados) => console.log(dados),
        error: (erro) => console.log(erro)
      }
    )
  }
}
