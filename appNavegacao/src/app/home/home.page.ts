import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  logo_miyas="../../../assets/imagens/logo_miyas.png"
  rotas = [
    {
      path: "/cad-autor",
      text: "Cadastrar Autor"
    },
    {
      path: "/cad-editora",
      text: "Cadastrar Editora"
    },
    {
      path: "/cad-livro",
      text: "Cadastrar Livro"
    },
    {
      path: "/cad-usuario",
      text: "Cadastrar Usuario"
    },
    {
      path: "/cad-pedidos",
      text: "Cadastrar Pedido"
    },
    {
      path: "/cad-funcionarios",
      text: "Cadastrar Funcionário"
    },
    {
      path: "/cad-endereco",
      text: "Cadastrar Endereço"
    }
  ]

  constructor() {}

}
