import { Component, OnInit } from '@angular/core';
import { Livro } from '../models/Livro';
import { LivrosService } from '../services/livros/livros.service';

@Component({
  selector: 'app-cad-livro',
  templateUrl: './cad-livro.page.html',
  styleUrls: ['./cad-livro.page.scss'],
})
export class CadLivroPage implements OnInit {

  livro: Livro

  constructor(
    private livroServices: LivrosService
  ) {
    this.livro = new Livro();
  }

  ngOnInit() {
  }

  cadastrar(): void {
    this.livroServices.salvar(this.livro).subscribe(
      {
        next: (dados) => console.log(dados),
        error: (erro) => console.log(erro)
      }
    )
  }
}
