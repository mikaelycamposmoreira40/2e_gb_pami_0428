import { Component, OnInit } from '@angular/core';
import { Pedido } from '../models/Pedido';
import { PedidoService } from '../services/pedidos/pedidos.service';


@Component({
  selector: 'app-cad-pedidos',
  templateUrl: './cad-pedidos.page.html',
  styleUrls: ['./cad-pedidos.page.scss'],
})
export class CadPedidosPage implements OnInit {

  pedido: Pedido

  constructor(
    private pedidosServices: PedidoService
  ) {
    this.pedido = new Pedido();
  }

  ngOnInit() {
  }

  cadastrar(): void{
    this.pedidosServices.cadastrarPedido(this.pedido).subscribe(
          {
            next:(dados)=>{
              console.log(dados)
            },
            error:(erro)=>
            {
              console.log(erro)
            }
          }
        )
    }
}