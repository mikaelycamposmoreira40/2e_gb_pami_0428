import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadPedidosPage } from './cad-pedidos.page';

const routes: Routes = [
  {
    path: '',
    component: CadPedidosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadPedidosPageRoutingModule {}
